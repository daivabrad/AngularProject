import { Component, OnInit } from '@angular/core';
import { Page, Track } from "../models/spotify-models";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.css']
})
export class TracksComponent implements OnInit {
  public result: Page<Track>;

  constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit() {
    // this.route.params.subscribe(parameters=>console.log(parameters['id'])
    this.route.params.subscribe(parameters=>{
      let id = parameters['id'];
      this.httpClient.get<Page<Track>>(
  'https://api.spotify.com/v1/albums/' + id + '/tracks'
).subscribe(apiResult=>this.result=apiResult); 
});

  }

}

