import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { SearchResult } from "../models/spotify-models";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  public result: SearchResult;
  constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      parameters=>{
      //q, nes formoje paminejom search?q=. SearchValue yra q(query)
      let q=parameters["q"];
      console.log(q);
      
      //requete HTTP
      this.httpClient.get<SearchResult>(
        //on recherche un artist avec request parameter q:
  'https://api.spotify.com/v1/search?type=artist&q=' + q
).subscribe(apiResult=>{
  console.log(apiResult); 
  this.result=apiResult;
      });
    }
   )
  }
}
