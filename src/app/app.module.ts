import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/app.component';
import { HomeComponent } from './components/home/home.component';
import { CallbackComponent } from './components/callback/callback.component';
import { OAuthInterceptor } from './interceptors/oauth-interceptor';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/nav/nav.component';
import { AlbumComponent } from "./components/album/album.component";
import { TracksComponent } from './tracks/tracks.component';
import { SearchComponent } from './search/search.component';
import { ArtistComponent } from './components/artist/artist.component';

const routes: Route[] = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'callback',
    component: CallbackComponent
  },
 {
    path:'album/:id',
    component: AlbumComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path:'artist/:id',
    component: ArtistComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CallbackComponent,
    NavComponent,
    AlbumComponent,
    TracksComponent,
    SearchComponent,
    ArtistComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,  //pour appler l'api
    FormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: OAuthInterceptor,
    multi: true,
  },
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
