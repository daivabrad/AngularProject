import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserProfile } from '../../models/spotify-models';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  userProfile: UserProfile;
  
    constructor(private authService: AuthService) { }
  
    ngOnInit(): void {
  
      this.authService.connectionStatusChanged.subscribe(res => {
        this.userProfile = res;
      }, err => {
        this.userProfile = null;
      });
    }
  
    signOut(): void {
  
      this.authService.signOut();
    }
}
