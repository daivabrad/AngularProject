import { Component, OnInit } from '@angular/core';
import { Artist, Page, Album } from "../../models/spotify-models";
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
public result: Artist;
public result1: Page<Album>;
   constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit() {
      this.route.params.subscribe(parameters=>{
      let id = parameters['id'];
      this.httpClient.get<Artist>(
  'https://api.spotify.com/v1/artists/' + id
).subscribe(apiResult=>this.result=apiResult); 

this.httpClient.get<Page<Album>>('https://api.spotify.com/v1/artists/' + id + '/albums'
).subscribe(apiResult=>this.result1=apiResult);
}
 )}
}
