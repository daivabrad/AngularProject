import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { UserProfile } from '../models/spotify-models';
import { AuthService } from '../services/auth.service';
import { Router } from "@angular/router";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
public searchValue:string;
  constructor(private authService: AuthService, 
       private router: Router) { }

  ngOnInit(): void {

    if (this.authService.hasValidToken()) {
      this.authService.signIn();
    }
  }

  public onSubmit():void{
    console.log(this.searchValue);
    //galima ir taip, ir taip:
    this.router.navigateByUrl('/search?q=' + this.searchValue);
   // this.router.navigate(['search'], 123);
  }
}
