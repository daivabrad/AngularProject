import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { SearchResult } from "../../models/spotify-models";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

public result: SearchResult;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    //cia mes pasakom, kad siunciam httpClient, kad jis mums duotu albumus, t.y. SearcgResult
this.httpClient.get<SearchResult>(
  'https://api.spotify.com/v1/browse/new-releases?limit=9'
  // ).subscribe(apiResult=> console.log(apiResult)); //get reiskia uzklausa, suscribe - tai, kad mes laukiam atsakymo
).subscribe(apiResult=>this.result=apiResult); //cia bus rodoma navigatoriuje, o ne konsoleje  
}
}
