import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Track, Page, Album } from "../../models/spotify-models";

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

public result: Album;

  constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit() {
    // this.route.params.subscribe(parameters=>console.log(parameters['id'])
    this.route.params.subscribe(parameters=>{
      let id = parameters['id'];
      this.httpClient.get<Album>(
  'https://api.spotify.com/v1/albums/' + id
).subscribe(apiResult=>this.result=apiResult); 
});

  }

}
